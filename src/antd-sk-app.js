
import { SkAppImpl }  from '../../sk-app/src/impl/sk-app-impl.js';

export class AntdSkApp extends SkAppImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'app';
    }

}
