# Skinny Widgets App for Antd Theme


app element

```
npm i sk-app sk-app-antd --save
```

then add the following to your html

```html
<sk-config
    theme="antd"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd"
    routes='{"page-a": {"PageAConfA": "/examples/spa/page-a-confa.js"}, "page-b": "page-b-confa"}'
></sk-config>
<sk-app></sk-app>
<script type="module">
    import { SkApp } from './node_modules/sk-app/index.js';

    customElements.define('sk-app', SkApp);
</script>
```

sk-app is route-to render component that can help you feel like with popular framework SPA app. It allows to map one or more
areas rendering to router path changes. It uses navigo library as runtime dependency to you have to have it loaded
before sk-widgets. 

Note: SkApp page load is implemented with dynamic imports browser feature in old browsers it will be switched to script 
tag append load. If route value not specified as string (custom element tag name) component load is 
performed on route change.

#### attributes

**dimport** - "false" switches page load from dynamic import to tag appending, also used ass fallback if dynamic imports 
supported was not detected in browser. Use it when loading classes from bundles.

**ro-root** - root attribute for Navigo, default: null

**ro-use-hash** - use-hash attribute for Navigo, default: true

**ro-hash** - hash attribute for Navigo, default: '#!'


#### events

**skroutestart** - triggers on route is started

**skrouteend** - triggers on route is done

```html
<script src="/node_modules/navigo/lib/navigo.min.js"></script>
<script>
    class PageAConfA extends HTMLElement {
        connectedCallback() {
            this.insertAdjacentHTML('beforeend', '<div>PageAConfA</div>');
        }
    }
    class PageAConfB extends HTMLElement {
        connectedCallback() {
            this.insertAdjacentHTML('beforeend', '<div>PageAConfB</div>');
        }
    }
    class PageBConfA extends HTMLElement {
        connectedCallback() {
            this.insertAdjacentHTML('beforeend', '<div>PageBConfA</div>');
        }
    }
    customElements.define('page-a-confa', PageAConfA);
    customElements.define('page-a-confb', PageAConfB);
    customElements.define('page-b-confa', PageBConfA);
</script>
<script src="/dist/sk-compat-bundle.js"></script>

<a href="page-a" data-navigo>Page A</a>
<a href="page-b" data-navigo>Page B</a>

<sk-config
        theme="antd"
        base-path="/node_modules/sk-core/src"
        theme-path="/node_modules/sk-theme-antd"
        lang="en"
        id="configA"
        routes='{"page-a": "page-a-confa", "page-b": "page-b-confa"}'
></sk-config>
<sk-app configSl="#configA"></sk-app>
<sk-config
        theme="antd"
        base-path="/node_modules/sk-core/src"
        theme-path="/node_modules/sk-theme-antd"
        lang="en"
        id="configB"
        routes='{"page-a": "page-a-confb", "page-b": "page-a-confb"}'
></sk-config>
<sk-app configSl="#configB"></sk-app>

</body>
```
Pages can be loaded automatically with the following mapping:

```html
<sk-config
        theme="antd"
        base-path="/node_modules/sk-core/src"
        theme-path="/node_modules/sk-theme-antd"
        lang="en"
        id="configA"
        routes='{"page-a": {"PageAConfA": "/examples/spa/page-a-confa.js"}, "page-b": "page-b-confa"}'
></sk-config>
```

You can attach your code to app route change with events:

```html
<sk-app id="skApp"></sk-app>
<script>
    skApp.addEventListener('skrouteend', function(event) {
        console.log('skrouteend handled', event);
    });
</script>
```


#### template

id: SkAppTpl